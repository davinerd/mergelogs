#!/usr/bin/env python

import os
import re
import datetime

time_format_full = '%Y-%m-%d %H:%M:%S.%f'
time_format_nousec = '%Y-%m-%d %H:%M:%S'
time_format_dateonly = '%Y-%m-%d'
time_format_errorlog = '[%a %b %d %H:%M:%S %Y]'
time_format_accesslog = '[%d/%b/%Y:%H:%M:%S'


def to_datetime(timestamp):
    '''
    Try to convert a timestamp to a datetime.datetime object. The input can be
    either a string, a datetime.datetime or None.
    '''
    if timestamp is None:
        return None
    if isinstance(timestamp, datetime.datetime):
        return timestamp
    for fmt in time_format_full, time_format_nousec, time_format_dateonly:
        try:
            return datetime.datetime.strptime(timestamp, fmt)
        except ValueError:
            continue
    return None


def timestamp_is_in_range(ts, start=None, end=None):
    '''
    Return whether a timestamp is in the given range.
    '''
    ts = to_datetime(ts)
    start = to_datetime(start)
    end = to_datetime(end)
    if start and ts < start:
            return False
    if end and ts > end:
        return False
    return True


class LogEntry(object):
    def __init__(self, timestamp=None, logfile=None, message='', prefix=''):
        self.timestamp = timestamp
        self.logfile = logfile
        self.message = message
        self.prefix = prefix
        self._num_lines = None

    @property
    def num_lines(self):
        if self._num_lines is None:
            self._num_lines = len(self.message.splitlines())
        return self._num_lines

    def __str__(self):
        return self.message

    def __repr__(self):
        return '<{cls} (timestamp={t!r}, logfile={l!r}, message={m!r})'.format(
            cls=self.__class__.__name__,
            t=self.timestamp,
            l=self.logfile,
            p=self.prefix,
            m=self.message,
        )

    def grep(self, pattern, regex=False):
        '''
        Search for a pattern in the log entry. If regex is True, the pattern is
        treated as a regular expression.
        '''
        if regex:
            rx = re.compile(pattern)
            return self if rx.match(self.message) is not None else None
        else:
            return self if pattern in self.message else None


class LogEntries(object):
    def __init__(self, entries, log_files=None):
        self.entries = entries
        self.log_files = log_files
        self.total_entries = len(entries)
        self.total_lines = sum([entry.num_lines for entry in entries])
        self._useful_log_files = None
        try:
            self.oldest = self.entries[0].timestamp
        except IndexError:
            self.oldest = None
        try:
            self.newest = self.entries[-1].timestamp
        except IndexError:
            self.newest = None

    @property
    def useful_log_files(self):
        if self._useful_log_files is None:
            self._useful_log_files = set([e.logfile for e in self.entries])
        return self._useful_log_files

    def __iter__(self):
        return iter(self.entries)

    def __getitem__(self, index):
        return self.entries[index]

    def __len__(self):
        return len(self.entries)

    def __repr__(self):
        return('<{cls} (total_entries={te!r}, total_lines={tl!r}, '
               'num_log_files={nlf!r})>'.format(
                   cls=self.__class__.__name__,
                   te=self.total_entries,
                   tl=self.total_lines,
                   nlf=len(self.log_files),
                   ))

    def __str__(self):
        return self.__repr__()

    def in_range(self, start, end):
        entries_in_range = []
        for entry in self.entries:
            if timestamp_is_in_range(entry.timestamp, start, end):
                entries_in_range.append(entry)
        return LogEntries(entries_in_range, self.log_files)

    def holes(self, hole_size=datetime.timedelta(seconds=5 * 60 * 60)):
        '''
        Find holes in the log files. Default hole size is 1 hour
        '''
        previous_entry = None
        holes = []
        for entry in self.entries:
            if previous_entry is None:
                previous_entry = entry
                continue
            if entry.timestamp - previous_entry.timestamp > hole_size:
                holes.append((previous_entry, entry))
            previous_entry = entry
        return holes

    def stats(self, verbose=False, as_dict=False):
        if as_dict:
            statsdict = {
                'total_lines': self.total_lines,
                'total_entries': self.total_entries,
                'oldest': self.oldest,
                'newest': self.newest,
            }
            if verbose:
                statsdict['holes'] = self.holes()
                statsdict['valid_logfiles'] = self.log_files
            return statsdict
        else:
            return self.printable_stats(verbose)

    def printable_stats(self, verbose=False):
        printable = '''Total log lines: {tl}
Total log entries: {te}
Oldest entry timestamp: {fet}
Newest entry timestamp: {let}
Total log files: {tlf}
Useful log files: {nlf}'''.format(
                  tl=self.total_lines,
                  te=self.total_entries,
                  fet=self.oldest,
                  let=self.newest,
                  tlf=len(self.log_files),
                  nlf=len(self.useful_log_files),
              )
        if verbose:
            # add details on the useful log files
            padding = ' ' * 8
            for log_file in sorted(self.log_files):
                printable += '\n{padding}{name}'.format(
                    padding=padding, name=log_file)
        printable += '''\nDiscarded log files: {dlf}'''.format(
            dlf=len(self.log_files) - len(self.useful_log_files)
                if self.log_files is not None else 'N/a'
        )
        if verbose:
            # add details on the discarded log files
            padding = ' ' * 8
            discarded_files = set(self.log_files).difference(
                set(self.useful_log_files))
            for log_file in sorted(discarded_files):
                printable += '\n{padding}{name}'.format(
                    padding=padding, name=log_file)
            # print holes in logs
            holes = self.holes()
            printable += '\nHoles (min 1h) found: {nh}\n'.format(nh=len(holes))
            for hole in holes:
                start = hole[0].timestamp
                end = hole[1].timestamp
                printable += '{padding}{start} ~ {end} (length: {l})\n'.format(
                    padding=padding,
                    start=start,
                    end=end,
                    l=end-start,
                )
        return printable

    def grep(self, pattern, regex=False):
        '''
        Search for a pattern in the log entry. If regex is True, the pattern is
        treated as a regular expression.
        '''
        ret = []
        for line in self.entries:
            match = line.grep(pattern, regex)
            if match:
                ret.append(match)
        return ret


def merge(filelist, pattern=None, is_regex=False):
    # if any of the files is a directory, merge all the files in the directory
    # too
    new_filelist = []
    for filename in filelist:
        if os.path.isdir(filename):
            dirname = filename
            new_filelist.extend(
                [os.path.join(dirname, fname) for fname in os.listdir(filename)]
            )
        else:
            new_filelist.append(filename)
    # build the file list and merge them
    prefix_and_file_list = []
    for filename in new_filelist:
        prefix_and_file_list.append((filename, filename))
    return merge_with_prefix(prefix_and_file_list, pattern, is_regex)


def extract_timestamp(line):
    timestamp = None
    try:
        ts_string = ' '.join(line.split()[:2])
        timestamp = datetime.datetime.strptime(
            ts_string, time_format_full)
    except ValueError:
        try:
            ts_string = ' '.join(line.split()[:5])
            timestamp = datetime.datetime.strptime(
                ts_string, time_format_errorlog,
            )
        except ValueError:
            try:
                ts_string = line.split()[3]
                timestamp = datetime.datetime.strptime(
                    ts_string, time_format_accesslog,
                )
            except (IndexError, ValueError):
                pass
    return timestamp


def merge_with_prefix(prefix_and_file_list, pattern=None, is_regex=False):
    '''
    Merge a set of log files and sort them by timestamp. Optionally return only
    the lines in the given time range.
    '''
    entries = []
    for prefix, filename in prefix_and_file_list:
        with open(filename, 'rb') as fd:
            entry = LogEntry(logfile=filename, prefix=prefix)
            for line in fd:
                if entry:
                    entry.message += line
                entry.timestamp = extract_timestamp(line)
                if entry.timestamp is not None:
                    if pattern:
                        if entry.grep(pattern, is_regex):
                            entries.append(entry)
                    else:
                        entries.append(entry)
                    entry = LogEntry(logfile=filename, prefix=prefix)

    sorted_entries = sorted(entries, key=lambda entry: entry.timestamp)
    return LogEntries(
        sorted_entries,
        log_files=[l[0] for l in prefix_and_file_list],
    )
