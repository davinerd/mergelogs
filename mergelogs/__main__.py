import argparse

from mergelogs import merge, to_datetime


def parse_args():
    parser = argparse.ArgumentParser(prog='mergelogs')
    parser.add_argument('files', nargs='+',
                        help='The input files or directories to merge. If an '
                             ' argument is a directory, all the files '
                             'contained at the first level are considered')
    parser.add_argument('--save', type=argparse.FileType('w'),
                        help='The merged and sorted log file')
    parser.add_argument('--start', default=None,
                        help='The start timestamp in '
                        'YYYY-mm-dd[ HH:MM:SS[.usec]] format')
    parser.add_argument('--end', default=None,
                        help='The end timestamp in '
                        'YYYY-mm-dd[ HH:MM:SS[.usec]] format')
    parser.add_argument('--stats', action='store_true', default=False,
                        help='Print some statistics on the logs')
    parser.add_argument('--verbose', action='store_true', default=False,
                        help='Print more verbose statistics')
    parser.add_argument('--shell', action='store_true', default=False,
                        help='Spawn an IPython shell after parsing')
    parser.add_argument('--grep', help='Filter by pattern, like grep')
    parser.add_argument('--regex', action='store_true',
                        help='Used with --grep, treats the pattern as a '
                        'regular expression')
    args = parser.parse_args()

    args.start = to_datetime(args.start)
    args.end = to_datetime(args.end)
    if args.regex and not args.grep:
        parser.error('Option --regex must be used in combination with --grep')
    return args


def main():
    args = parse_args()
    total_entries = merge(args.files, args.grep, args.regex)
    entries_in_range = total_entries.in_range(args.start, args.end)
    if args.save:
        fd = args.save
        outfile = args.save.name
    else:
        outfile = 'merged.log'
        fd = open(outfile, 'w')
    for entry in entries_in_range:
        fd.write('{p} {m}'.format(p=entry.prefix, m=entry.message))

    if args.stats:
        print('Statistics')
        print('=' * 80)
        print entries_in_range.stats(verbose=args.verbose)
        print('-' * 80)
        print('Excluded log entries: {n}'.format(
            n=len(total_entries) - len(entries_in_range)))

    print('Log entries saved to {f}. Time range: ({s}, {e})'
          .format(
            f=outfile,
            s=args.start,
            e=args.end,
          )
          )

    if args.shell:
        import IPython
        # this assignment is for visibility of a short-named variable in the
        # IPython shell
        logs = total_entries
        IPython.embed()


if __name__ == '__main__':
    main()
