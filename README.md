# README

## What is it?

This is a very simple command-line tool written in Python that helps the analysis of OpenStack logs. I have created it to help me with an assignment of COMP40750 - Corporate Investigations for my [M.Sc. in Digital Investigations and Forensic Computing](https://www.cs.ucd.ie/PostgraduateProgrammes/MSc_DIFC/) at [UCD](https://www.ucd.ie).

This is a weekend project (actually I developed it on a plane) and the code is really ugly. Please be kind and bear with me :)

## Dependencies

* The [Python](https://www.python.org/) programming environment
* [optional] [IPython](http://ipython.org/) for the shell features


## How to install it

### Download it

Either download the zipball at https://bitbucket.org/insomniacslk/mergelogs/get/v0.1.zip or get the development branch via git with

```bash
git clone https://insomniacslk@bitbucket.org/insomniacslk/mergelogs.git
```

### Install it
If you downloaded the zipball, unpack it somewhere in your system, then open a terminal and enter the directory that it created. To install it run the following command:

```bash
python setup.py install # as root, or add --user for local installation.
                        # If you use OSX, you need "--user --prefix=" to work around a system bug
```

If you are on Windows, you will probably have to run something like this:

```bash
C:\Python27\python.exe setup.py install
```

or have the path to python.exe in your system's PATH and just call ```python.exe```.

## Features

The tool can:

* merge various types of logs, including multi-line log entries
* filter the logs in a specific time range
* print some statistics, like the number of log entries and log lines, the files
  that contributed to the log analysis, and the *holes* that log files have.
* search and filter by expressions, including [regular
  expressions](https://en.wikipedia.org/wiki/Regular_expression)
* save the resulting logs to a file
* drop into an IPython shell for further manual analysis. Requires
  [IPython](http://ipython.org/) installed


## How to use it

The next commands assume you have a UNIX-like environment. If you have Windows, replace the ```python``` command with something like ```C:\Python27\python.exe```.

### Show the usage instructions

Use use ```--help```:

```bash
python -m mergelogs.py --help
usage: mergelogs.py [-h] [--save SAVE] [--start START] [--end END] [--stats]
                    [--verbose] [--shell] [--grep GREP] [--regex]
                    files [files ...]

positional arguments:
  files          The input files or directories to merge. If an argument is a
                 directory, all the files contained at the first level are
                 considered

optional arguments:
  -h, --help     show this help message and exit
  --save SAVE    The merged and sorted log file
  --start START  The start timestamp in YYYY-mm-dd[ HH:MM:SS[.usec]] format
  --end END      The end timestamp in YYYY-mm-dd[ HH:MM:SS[.usec]] format
  --stats        Print some statistics on the logs
  --verbose      Print more verbose statistics
  --shell        Spawn an IPython shell after parsing
  --grep GREP    Filter by pattern, like grep
  --regex        Used with --grep, treats the pattern as a regular expression
```

### Merge and print some stats

Use ```--stats```

```bash
python -m mergelogs.py logs/* --stats
Statistics
================================================================================
Total log lines: 60788
Total log entries: 49094
Oldest entry timestamp: 2014-04-16 20:40:48.613000
Newest entry timestamp: 2014-04-22 12:04:38.208000
Useful log files: 18
--------------------------------------------------------------------------------
Excluded log entries: 0
Log entries saved to merged.log. Time range: (None, None)
```

**NOTE** on the DOS shell in Windows you cannot use wildcards. Either use the
PowerShell, or copy all the log files in a subdirectory and use it as a target.

### Print more verbose statistics

For example, the holes found in the logs. Use ```--verbose```:


```bash
python -m mergelogs.py logs/* --stats --verbose
Statistics
================================================================================
Total log lines: 60788
Total log entries: 49094
Oldest entry timestamp: 2014-04-16 20:40:48.613000
Newest entry timestamp: 2014-04-22 12:04:38.208000
Useful log files: 18
        logs/all.log
        logs/api.log
        logs/cinder-api.log
        logs/cinder-scheduler.log
        logs/cinder-volume.log
        logs/error.log
        logs/nova-api-metadata.log
        logs/nova-api.log
        logs/nova-cert.log
        logs/nova-compute.log
        logs/nova-conductor.log
        logs/nova-consoleauth.log
        logs/nova-dhcpbridge.log
        logs/nova-manage_csi.log
        logs/nova-manage_lbg.log
        logs/nova-network.log
        logs/nova-scheduler.log
        logs/registry.log
Holes (min 1h) found: 2
        2014-04-16 21:48:37.116000 ~ 2014-04-17 09:36:23.295000 (length: 11:47:46.179000)
        2014-04-17 18:15:43.977000 ~ 2014-04-21 14:24:08.113000 (length: 3 days, 20:08:24.136000)

--------------------------------------------------------------------------------
Excluded log entries: 0
Log entries saved to merged.log. Time range: (None, None)
```

### Merge and filter logs in a time range

If you want to look for activity between 00:00 and 12:00 of the 21st of April 2014, so run:

```bash
python -mmergelogs.py --start '2014-04-21' --end '2014-04-21 12:00:00' logs/* --stats --verbose
Statistics
================================================================================
Total log lines: 0
Total log entries: 0
Oldest entry timestamp: None
Newest entry timestamp: None
Useful log files: 0
Holes (min 1h) found: 0

--------------------------------------------------------------------------------
Excluded log entries: 49094
Log entries saved to merged.log. Time range: (2014-04-21 00:00:00, 2014-04-21 12:00:00)
```

### Filter logs by content

There is a grep-like functionality embedded in the tool. Just use ```--grep <your-pattern>``` . You can
also tell the parser that the pattern is a regular expression by using ```--regex```.
For example:

```bash
$ ./mergelogs.py logs/* --grep login --stats
Statistics
================================================================================
Total log lines: 208
Total log entries: 208
Oldest entry timestamp: 2014-04-17 10:38:58
Newest entry timestamp: 2014-04-22 11:53:48
Useful log files: 3
--------------------------------------------------------------------------------
Excluded log entries: 0
Log entries saved to merged.log. Time range: (None, None)
```

```bash
python -m mergelogs.py logs/* --grep '.*/static/.+\.png' --regex --stats
Statistics
================================================================================
Total log lines: 123
Total log entries: 123
Oldest entry timestamp: 2014-04-17 10:23:56
Newest entry timestamp: 2014-04-22 10:47:31
Useful log files: 1
--------------------------------------------------------------------------------
Excluded log entries: 0
Log entries saved to merged.log. Time range: (None, None)
```

### Drop into the IPython shell

You can also use the shell to retrieve the same information. Run ```python -m mergelogs.py logs/* --shell``` and you'll drop in an IPython shell. In the shell, run the following commands:

```python
In [1]: print logs.in_range('2014-04-21 00:00:00', '2014-04-21 12:00:00').stats(verbose=True)
Total log lines: 0
Total log entries: 0
Oldest entry timestamp: None
Newest entry timestamp: None
Useful log files: 0
Holes (min 1h) found: 0
```

Or, if you want a machine-useable data format, you can have a dictionary with ```as_dict=True``` passed to ```.stats()```:

```python
In [2]: logs.stats(verbose=True, as_dict=True)
Out[2]:
{'holes': [(<LogEntry (timestamp=datetime.datetime(2014, 4, 16, 21, 48, 37, 116000), logfile='logs/nova-dhcpbridge.log', message='2014-04-16 21:48:37.116 19437 INFO nova.openstack.common.rpc.common [req-ec14df42-6f7f-4ef4-9ddc-43eed4fc7821 None None] Reconnecting to AMQP server on csi:5672\n'),
   <LogEntry (timestamp=datetime.datetime(2014, 4, 17, 9, 36, 23, 295000), logfile='logs/api.log', message='2014-04-17 09:36:23.295 1419 ERROR glance.store.sheepdog [-] Error in store configuration: Unexpected error while running command.\n')),
  (<LogEntry (timestamp=datetime.datetime(2014, 4, 17, 18, 15, 43, 977000), logfile='logs/cinder-volume.log', message='2014-04-17 18:15:43.977 1457 INFO cinder.service [-] Child 1576 killed by signal 15\n'),
   <LogEntry (timestamp=datetime.datetime(2014, 4, 21, 14, 24, 8, 113000), logfile='logs/api.log', message='2014-04-21 14:24:08.113 1394 ERROR glance.store.sheepdog [-] Error in store configuration: Unexpected error while running command.\n'))],
 'newest': datetime.datetime(2014, 4, 22, 12, 4, 38, 208000),
 'oldest': datetime.datetime(2014, 4, 16, 20, 40, 48, 613000),
 'total_entries': 49094,
 'total_lines': 60788,
 'valid_logfiles': {'logs/all.log',
  'logs/api.log',
  'logs/cinder-api.log',
  'logs/cinder-scheduler.log',
  'logs/cinder-volume.log',
  'logs/error.log',
  'logs/nova-api-metadata.log',
  'logs/nova-api.log',
  'logs/nova-cert.log',
  'logs/nova-compute.log',
  'logs/nova-conductor.log',
  'logs/nova-consoleauth.log',
  'logs/nova-dhcpbridge.log',
  'logs/nova-manage_csi.log',
  'logs/nova-manage_lbg.log',
  'logs/nova-network.log',
  'logs/nova-scheduler.log',
  'logs/registry.log'}}
```

## What's missing?

* the support for timezones. At the moment it supports only naive timestamps
* pluggable parser backends instead of a horrible try..except chain
